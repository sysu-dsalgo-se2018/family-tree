#ifndef COMPILER_H
#define COMPILER_H

#include <vector>
#include <algorithm>
#include <functional>
#include <stdexcept>
#include <cstdlib>
#include <map>
#include <set>
#include <iostream>
#include <memory>
#include <sstream>
#include <utility>
#include <QString>
#include <QTextStream>

#ifdef _MSC_VER
#define NORETURN __declspec(noreturn)
#elif defined __GNUC__
#define NORETURN __attribute__((noreturn))
#else
#define NORETURN
#endif

namespace compiler
{
    double fmod(double _X, double _Y)
    {
        return _X - (int)(_X / _Y) * _Y;
    }
} // namespace compiler

namespace compiler
{
    using namespace std;

    template <typename T, typename... Args>
    std::function<bool(T)> is_any_of(T t0, Args... args)
    {
        T t[] = { t0, args... };
        return [=](const T &a) {
            for (auto &i : t)
                if (i == a)
                    return true;
            return false;
        };
    }

    template <typename StreamT>
    void format0(StreamT &ss, QStringRef fmt)
    {
        ss << fmt;
    }

    template <typename StreamT, typename T, typename... Args>
    void format0(StreamT &ss, QStringRef fmt, const T &arg0, Args... args)
    {
        if (fmt.isEmpty())
            return;
        if (fmt[0] == '{' && fmt[1] == '}')
        {
            ss << arg0;
            format0(ss, fmt.mid(2), args...);
        }
        else
        {
            ss << fmt[0];
            format0(ss, fmt.mid(1), arg0, args...);
        }
    }

    template <typename... Args>
    QString format(QString fmt, Args... args)
    {
        QString str;
        QTextStream ss(&str);
        format0(ss, QStringRef(&fmt), args...);
        return str;
    }

    template <typename U>
    QString to_string(const U &u)
    {
        QString str;
        QTextStream ss(&str);
        ss << u;
        return str;
    }

    struct bad_lexical_cast : public exception
    {
    };

    template <typename T>
    T lexical_cast(const QString &str)
    {
        T out;
        stringstream ss;
        ss << str.toStdString();
        ss >> out;
        if (ss.rdbuf()->in_avail())
            throw bad_lexical_cast();
        else
            return out;
    }

    void to_lower(QString &str)
    {
        for (QString::iterator it = str.begin(); it != str.end(); ++it)
            *it = it->toLower();
    }

    bool is_name_char(QChar ch)
    {
        return ch.isLetterOrNumber() || ch == '_';
    }

    bool is_name(QString name)
    {
        if (name.isEmpty())
            return false;

        if (name.size() > 1 && *name.begin() == '`' && *name.rbegin() == '`')
            return true;

        if (name[0].isDigit())
            return false;

        for (QChar c : name)
            if (!is_name_char(c))
                return false;

        return true;
    }

    bool is_constant(const QString &token)
    {
        return !token.isEmpty() && (token[0].isDigit() || token[0] == '\'' || token[0] == '"' || token == "false" || token == "true");
    }

    int is_symbol(QChar s1, QChar s2)
    {
        static QString SYMBOLS[] = { "<=", ">=", "!=", "&&", "||", "<<", ">>" };

        QString w2;
        w2 += s1;
        w2 += s2;

        for (QString i : SYMBOLS)
        {
            if (i == w2)
                return 2;
        }
        return 1;
    }

    bool parse_constant_string(QString pattern, QString &out)
    {
        out = pattern;
        return true;
    }

    struct compilation_error : public exception
    {
        QString message;

        compilation_error() : exception() {}
        compilation_error(const QString &message) : message(message) {}

        char const *what() const noexcept override
        {
            return message.toStdString().c_str();
        }
    };

    class program
    {
    public:
        explicit program(const QString &code)
            : code(code) {}

        const QString &get_code() const
        {
            return code;
        }

        template <typename PosT, typename... Args>
        NORETURN void compilation_error(PosT const &t, const char *fmt, Args... args) const
        {
            throw compiler::compilation_error(format("错误位置:{}:{}:", t.line, t.column) + format(fmt, args...));
        }

        template <typename... Args>
        NORETURN void compilation_error(int line, int column, const char *fmt, Args... args) const
        {
            throw compiler::compilation_error(format("错误位置:{}:{}:", line, column) + format(fmt, args...));
        }

    private:
        QString code;
    };

    class object_t
    {
        struct base
        {
            virtual unique_ptr<base> clone() const = 0;
        };

        template <typename T>
        struct derived : base
        {
            T value;

            derived(const T &value) : value(value) {}

            unique_ptr<base> clone() const override
            {
                return unique_ptr<base>(new derived<T>(value));
            }
        };

        unique_ptr<base> clone() const
        {
            if (!empty())
                return ptr->clone();
            else
                return nullptr;
        }

        unique_ptr<base> ptr;

    public:
        object_t() {}
        object_t(const object_t &that) : ptr(that.clone()) {}

        template <typename T>
        object_t(const T &value) : ptr(new derived<T>(value)) {}

        bool empty() const
        {
            return ptr == nullptr;
        }

        template <typename U>
        bool is() const
        {
            return dynamic_cast<derived<U> *>(ptr.get());
        }

        template <typename U>
        U cast() const
        {
            derived<U> *d = dynamic_cast<derived<U> *>(ptr.get());
            if (!d)
                throw bad_cast();
            return d->value;
        }

        object_t &operator=(const object_t &b)
        {
            if (ptr != b.ptr)
            {
                ptr = b.clone();
            }
            return *this;
        }

        template <typename T>
        bool operator==(const T &b) const
        {
            if (empty())
                return false;
            else
            {
                auto d = dynamic_cast<derived<T> *>(ptr.get());
                if (!d)
                    return false;
                else
                    return d->value == b;
            }
        }
    };

    template <>
    QString to_string<object_t>(const object_t &t)
    {
        if (t.is<double>())
            return to_string(t.cast<double>());
        else if (t.is<QString>())
            return t.cast<QString>();
        else
            return ""; // never happens
    }

    double to_double(const object_t &t)
    {
        if (t.is<double>())
            return t.cast<double>();
        else if (t.is<QString>())
            return lexical_cast<double>(t.cast<QString>());
        else
            return 0.0; // never happens
    }

    struct variables
    {
        map<QString, QString> data;
        void set_data(const QString &col, const QString &datum)
        {
            data[col] = datum;
        }

        QString get_data(const QString &col) const
        {
            return !data.count(col) ? "" : data.at(col);
        }
    };

    struct table_structure
    {
        QString name;
        vector<QString> columns;

        table_structure(const QString &name, const vector<QString> &columns)
            : name(name), columns(columns) {}

        bool has_column(const QString &name) const
        {
            for (auto &i : columns)
                if (i == name)
                    return true;
            return false;
        }
    };

    struct database_structure
    {
        vector<table_structure *> tables;

        database_structure(const vector<table_structure *> &tables)
            : tables(tables) {}

        table_structure *table(const QString &table_name) const
        {
            for (auto &i : tables)
                if (i->name == table_name)
                    return i;
            throw runtime_error(format("找不到表格`{}`", table_name).toStdString().c_str());
        }
    };

    class token
    {
    public:
        token() : token("", 0, 0) {}

        token(const QString &code, int line_number, int line_column)
            : code(code), line(line_number), column(line_column) {}

        QString code;
        int line;
        int column;
    };

    class lexical_analyzer
    {
    public:
        explicit lexical_analyzer(const program &prog)
            : prog(prog), code(prog.get_code())
        {
        }

        std::vector<token> analyze()
        {
            std::vector<token> result;
            token t;
            while (t = next_token(), !t.code.isEmpty())
                result.push_back(t);
            return result;
        }

    private:
        const program &prog;
        QString code;
        int cur = 0, line_number = 1, line_column = 0;

        QChar next_char()
        {
            QChar ch = cur < code.length() ? (line_column++, QChar(code[cur++])) : QChar(QChar::Null);
            if (ch == '\n')
                line_number++, line_column = 0;
            return ch;
        }

        QChar peek_char(int offset = 0)
        {
            return cur + offset < (int)code.size() ? QChar(code[cur + offset]) : QChar(QChar::Null);
        }

        token next_token()
        {
            QString token;
            QChar c = next_char();
            while (c.isSpace())
                c = next_char();

            int num = line_number, col = line_column;

            if (!c.isNull())
            {
                token += c;
                if (c == '"' || c == '\'' || c == '`')
                {
                    QChar cur = c;
                    bool escape = false;
                    while (c = next_char(), true)
                    {
                        if (c.isNull())
                            prog.compilation_error(line_number, line_column, "期望字符串，但 SQL 语句意外结束", cur);

                        token += c;

                        if (!escape)
                        {
                            if (c == cur)
                                break;
                            else if (c == '\\')
                                escape = true;
                        }
                        else
                            escape = false;
                    }

                    QString temp;
                    if (!parse_constant_string(token, temp))
                        prog.compilation_error(line_number, line_column, "不是一个合法的字符串常量");

                    if (c == '`')
                        to_lower(token);
                }
                else if (c.isDigit())
                {
                    while (c = peek_char(), c.isDigit() || c == '.')
                        token += next_char();

                    if (c == 'e' || c == 'E')
                    {
                        token += next_char();
                        if (c = peek_char(), c == '+' || c == '-')
                            token +=next_char();
                        if (!peek_char().isDigit())
                            prog.compilation_error(line_number, line_column, "期望浮点数，但意外遇到 `{}`", c);
                        while (c = peek_char(), is_name_char(c))
                            token += next_char();
                    }
                    else
                        while (c = peek_char(), is_name_char(c))
                            token += next_char();
                    to_lower(token);
                }
                else if (!is_name_char(c))
                {
                    int len = is_symbol(c, peek_char(0));
                    while (--len > 0)
                        token += next_char();
                }
                else
                {
                    while (c = peek_char(), is_name_char(c))
                        token += next_char();
                    to_lower(token); // SQL is case insensitive.
                }
            }

            return compiler::token(token, num, col);
        }
    };

    struct built_in_function
    {
        virtual object_t call(const variables &var, const std::vector<object_t> &vec) const = 0;
    };

    struct built_in_starts_with : public built_in_function
    {
        virtual object_t call(const variables &var, const std::vector<object_t> &vec) const override
        {
            if (vec.size() != 2)
                throw compilation_error("函数 startsWith 需要1个字符串");
            QString a = to_string(vec[0]);
            QString b = to_string(vec[1]);
            return (double) a.startsWith(b);
        }
    };

    struct built_in_ends_with : public built_in_function
    {
        virtual object_t call(const variables &var, const std::vector<object_t> &vec) const override
        {
            if (vec.size() != 2)
                throw compilation_error("函数 endsWith 需要1个字符串");
            QString a = to_string(vec[0]);
            QString b = to_string(vec[1]);
            return (double) a.endsWith(b);
        }
    };

    template <typename T>
    struct mapper
    {
        virtual T map(variables &) const = 0;
    };

    struct mapper_expr : public mapper<object_t>
    {
    };

    // Children: Expression
    struct mapper_assign : public mapper<void>
    {
        QString name;
        unique_ptr<mapper_expr> exp;

        mapper_assign(const QString &name, unique_ptr<mapper_expr> &&exp) : name(name), exp(move(exp)) {}

        void map(variables &row) const override
        {
            row.set_data(name, to_string(exp->map(row)));
        }
    };

    // Children: Assign
    struct mapper_assignments : public mapper<void>
    {
        vector<unique_ptr<mapper_assign>> assigns;

        mapper_assignments(vector<unique_ptr<mapper_assign>> &&assigns) : assigns(move(assigns)) {}

        void map(variables &row) const override
        {
            for (auto &&i : assigns)
                i->map(row);
        }
    };

    // Children: Boolean Expression
    struct mapper_where : public mapper<bool>
    {
        unique_ptr<mapper_expr> bool_expr;

        mapper_where(unique_ptr<mapper_expr> &&bool_expr) : bool_expr(move(bool_expr)) {}

        bool map(variables &row) const override
        {
            object_t result = bool_expr->map(row);
            if (result.is<double>())
                return result.cast<double>();
            else if (result.is<QString>())
                return !result.cast<QString>().isEmpty();
            else
                return false;
        }
    };

    // Children: Constant Expressions
    struct mapper_values : public mapper<vector<object_t>>
    {
        vector<unique_ptr<mapper_expr>> exp;

        mapper_values(vector<unique_ptr<mapper_expr>> &&exp) : exp(move(exp)) {}

        vector<object_t> map(variables &row) const override
        {
            vector<object_t> values;
            for (auto &&i : exp)
                values.push_back(i->map(row));
            return values;
        }
    };

    struct mapper_var : public mapper_expr
    {
        QString name;

        mapper_var(const QString &name) : name(name) {}

        object_t map(variables &row) const override
        {
            return row.get_data(name);
        }
    };

    object_t eval_constant(const QString &value)
    {
        if (value[0] == '\'' || value[0] == '"') // QString
        {
            QString result;
            if (!parse_constant_string(value.mid(1, value.size() - 2), result))
                return object_t();
            else
                return result;
        }
        else if (value == "true")
            return 1.0;
        else if (value == "false")
            return 0.0;
        else
        {
            try
            {
                int idx = value.indexOf("e");
                if (idx != -1)
                {
                    double m = lexical_cast<double>(value.mid(0, idx));
                    int e = lexical_cast<int>(value.mid(idx + 1));
                    if (e > 0)
                        for (int i = 0; i < e; ++i)
                            m *= 10;
                    else if (e < 0)
                        for (int i = 0; i < -e; ++i)
                            m /= 10;
                    return m;
                }
                else
                {
                    return lexical_cast<double>(value);
                }
            }
            catch (bad_lexical_cast &)
            {
                return 0.0;
            }
        }
    }

    struct mapper_constant : public mapper_expr
    {
        object_t value;

        mapper_constant(const object_t &value) : value(value) {}

        object_t map(variables &) const override
        {
            return value;
        }
    };

    template <typename T>
    int eval_binary_comparison(const QString &op, const object_t &lhs, const object_t &rhs)
    {
        T l = lhs.cast<T>(), r = rhs.cast<T>();
        if (op == "=")
            return l == r;
        else if (op == "!=" || op == "<>")
            return l != r;
        else if (op == ">")
            return l > r;
        else if (op == "<")
            return l < r;
        else if (op == "<=")
            return l <= r;
        else if (op == ">=")
            return l >= r;
        else
            return -1;
    }

    template <typename T>
    object_t eval_binary(const QString &op, const object_t &lhs, const object_t &rhs)
    {
        T l = lhs.cast<T>(), r = rhs.cast<T>();
        long long L = (long long)(l + 0.5), R = (long long)(r + 0.5);
        if (op == "*")
            return T(l * r);
        else if (op == "+")
            return T(l + r);
        else if (op == "-")
            return T(l - r);
        else if (op == "/")
            return T(l / r);
        else if (op == "<<")
            return T(L << R);
        else if (op == ">>")
            return T(L >> R);
        else if (op == "&&" || op == "and")
            return T(L && R);
        else if (op == "||" || op == "or")
            return T(L || R);
        else if (op == "^")
            return T(L ^ R);
        else if (op == "|")
            return T(L | R);
        else if (op == "&")
            return T(L & R);
        else if (op == "%" || op == "mod")
            return T(fmod(l, r));
        else
            return object_t();
    }

    inline void convert_potential_string(object_t &val)
    {
        if (val.is<QString>())
            val = eval_constant(val.cast<QString>());
    }

    // Children: Expression, Expression
    struct mapper_binary_operator : public mapper_expr
    {
        QString op;
        unique_ptr<mapper_expr> lhs;
        unique_ptr<mapper_expr> rhs;

        mapper_binary_operator(const QString &op, unique_ptr<mapper_expr> &&lhs, unique_ptr<mapper_expr> &&rhs)
            : op(op), lhs(move(lhs)), rhs(move(rhs)) {}

        object_t map(variables &row) const override
        {
            object_t res, l = lhs->map(row), r = rhs->map(row);
            if (l.is<QString>() && r.is<QString>())
            {
                int cmp = eval_binary_comparison<QString>(op, l, r);
                if (cmp != -1)
                    return (double)cmp;
            }
            convert_potential_string(l);
            convert_potential_string(r);

            int cmp = eval_binary_comparison<double>(op, l, r);
            if (cmp != -1)
                return (double)cmp;
            else
                return eval_binary<double>(op, l, r);
        }
    };

    template <typename T>
    object_t eval_unary(const QString &op, const object_t &obj)
    {
        T val = obj.cast<T>();
        if (op == "-")
            return -val;
        else if (op == "+")
            return +val;
        else if (op == "!")
            return (double)!val;
        else if (op == "~")
            return (double)(~((long long)val));
        else
            return object_t();
    }

    // Children: Expression
    struct mapper_unary_operator : public mapper_expr
    {
        QString op;
        unique_ptr<mapper_expr> operand;

        mapper_unary_operator(const QString &op, unique_ptr<mapper_expr> &&operand)
            : op(op), operand(move(operand)) {}

        object_t map(variables &row) const override
        {
            object_t res, val = operand->map(row);
            convert_potential_string(val);

            res = eval_unary<double>(op, val);
            return res;
        }
    };

    template <typename T>
    bool compare_in(const T &t, vector<object_t> const &values_eval)
    {
        for (auto &cmp : values_eval)
            if (cmp == t)
                return true;
        return false;
    }

    struct mapper_in : public mapper_expr
    {
        unique_ptr<mapper_expr> lhs;
        unique_ptr<mapper_values> values;

        mapper_in(unique_ptr<mapper_expr> &&lhs, unique_ptr<mapper_values> &&values)
            : lhs(move(lhs)), values(move(values)) {}

        object_t map(variables &row) const override
        {
            object_t obj = lhs->map(row);
            vector<object_t> values_eval = values->map(row);
            obj = to_string(obj);
            for (auto &i : values_eval)
                i = to_string(i);
            return (double)compare_in(obj.cast<QString>(), values_eval);
        }
    };

    template <typename T>
    bool compare_between(const object_t &l, const object_t &m, const object_t &h)
    {
        T mid = m.cast<T>();
        return l.cast<T>() <= mid && mid <= h.cast<T>();
    }

    struct mapper_between : public mapper_expr
    {
        unique_ptr<mapper_expr> value, low, high;

        mapper_between(unique_ptr<mapper_expr> &&value, unique_ptr<mapper_expr> &&low, unique_ptr<mapper_expr> &&high)
            : value(move(value)), low(move(low)), high(move(high)) {}

        object_t map(variables &row) const override
        {
            object_t obj = value->map(row);
            object_t l = low->map(row);
            object_t h = high->map(row);
            if (obj.is<double>() || l.is<double>() || h.is<double>())
            {
                convert_potential_string(obj);
                convert_potential_string(l);
                convert_potential_string(h);
                return (double)compare_between<double>(l, obj, h);
            }
            else
            {
                return (double)compare_between<QString>(l, obj, h);
            }
        }
    };

    // Children: Callee, Args...
    struct mapper_func_call : public mapper_expr
    {
        const built_in_function &func;
        vector<unique_ptr<mapper_expr>> args;

        mapper_func_call(const built_in_function &func, vector<unique_ptr<mapper_expr>> &&args)
            : func(func), args(move(args)) {}

        object_t map(variables &row) const override
        {
            vector<object_t> arg_values;
            for (auto &&i : args)
                arg_values.push_back(i->map(row));
            object_t res = func.call(row, arg_values);
            qDebug() << to_string(res);
            return res;
        }
    };

    class syntax_analyzer
    {
        program &prog;
        vector<token> tokens;
        int ptr = 0;
        vector<pair<QString, const built_in_function &>> declared_functions;

        void expect_code(const token &actual, const QString &expected)
        {
            if (actual.code != expected)
                prog.compilation_error(actual, "需要 `{}`, 但找到 `{}`", expected, actual.code);
        }

        unique_ptr<mapper_expr> parse_binary_operator(function<unique_ptr<mapper_expr>()> inside, function<bool(QString)> predicate)
        {
            unique_ptr<mapper_expr> result = inside();
            while (predicate(peek_token_str()))
            {
                auto t = next_token();

                unique_ptr<mapper_expr> lhs = move(result);
                unique_ptr<mapper_expr> rhs = inside();

                result = unique_ptr<mapper_expr>(new mapper_binary_operator(t.code, move(lhs), move(rhs)));
            }
            return move(result);
        }

        unique_ptr<mapper_expr> parse_unary_operator(function<unique_ptr<mapper_expr>()> inside, function<bool(QString)> predicate)
        {
            if (predicate(peek_token_str()))
            {
                auto t = next_token();
                unique_ptr<mapper_expr> operand = inside();

                return unique_ptr<mapper_expr>(new mapper_unary_operator(t.code, move(operand)));
            }
            else
                return inside();
        }

    public:
        syntax_analyzer(program &prog, vector<token> tokens, vector<pair<QString, const built_in_function &>> declared_functions)
            : prog(prog), tokens(tokens), declared_functions(declared_functions) {}

        bool peek_token_is(const QString &token) const
        {
            return peek_token_str() == token;
        }

        QString peek_token_str(int offset = 0) const
        {
            return peek_token(offset).code;
        }

        token peek_token(int offset = 0) const
        {
            return ptr + offset >= (int)tokens.size() ? token() : tokens[ptr + offset];
        }

        QString next_name_token()
        {
            token t = next_token();
            QString name = t.code;
            if (!is_name(name))
                prog.compilation_error(t, "需要标识符（列名、表名），但找到 {}", name);
            if (*name.begin() == '`' && *name.rbegin() == '`' && name.size() > 1)
                name = name.mid(1, name.size() - 2);
            return name;
        }

        token next_token()
        {
            return ptr >= (int)tokens.size() ? token() : tokens[ptr++];
        }

        void assert_next_token(const QString &expected)
        {
            token actual = next_token();
            expect_code(actual, expected);
        }

        unique_ptr<mapper_where> analyze()
        {
            return unique_ptr<mapper_where>(new mapper_where(parse_expression()));
        }

        unique_ptr<mapper_values> parse_values()
        {
            vector<unique_ptr<mapper_expr>> values;
            do
            {
                unique_ptr<mapper_expr> res = parse_expression();
                values.push_back(move(res));
                if (!peek_token_is(","))
                    break;
                assert_next_token(",");
            } while (true);

            return unique_ptr<mapper_values>(new mapper_values(move(values)));
        }

        unique_ptr<mapper_expr> parse_expression()
        {
            return parse_unit15();
        }

        unique_ptr<mapper_expr> parse_unit0()
        {
            token name_token = next_token();
            QString name = name_token.code;

            if (is_constant(name))
                return unique_ptr<mapper_expr>(new mapper_constant(eval_constant(name)));
            else if (is_name(name))
            {
                if (name[0] == '`')
                    name = name.mid(1, name.size() - 2);

                if (peek_token_is("(")) // function call
                {
                    vector<unique_ptr<mapper_expr>> args;
                    assert_next_token("(");
                    if (!peek_token_is(")"))
                    {
                        while (true)
                        {
                            args.push_back(move(parse_unit15())); // No comma expression in func call args.
                            if (peek_token_is(","))
                                assert_next_token(",");
                            else // )
                                break;
                        }
                    }
                    assert_next_token(")");
                    for (auto &i : declared_functions)
                        if (i.first == name)
                            return unique_ptr<mapper_expr>(new mapper_func_call(i.second, move(args)));
                    prog.compilation_error(name_token, "找不到函数 `{}`", name);
                    return nullptr;
                }
                else
                {
                    return unique_ptr<mapper_expr>(new mapper_var(name));
                }
            }
            else
            {
                expect_code(name_token, "(");
                unique_ptr<mapper_expr> result = parse_expression();
                assert_next_token(")");
                return result;
            }
        }

        unique_ptr<mapper_expr> parse_unit3()
        {
            return parse_unary_operator([this]() { return parse_unit0(); }, is_any_of<QString>("+", "-", "!", "~"));
        }

        unique_ptr<mapper_expr> parse_unit4()
        {
            return parse_binary_operator([this]() { return parse_unit3(); }, is_any_of<QString>("^"));
        }

        unique_ptr<mapper_expr> parse_unit5()
        {
            return parse_binary_operator([this]() { return parse_unit4(); }, is_any_of<QString>("*", "/", "%", "div", "mod"));
        }

        unique_ptr<mapper_expr> parse_unit6()
        {
            return parse_binary_operator([this]() { return parse_unit5(); }, is_any_of<QString>("+", "-"));
        }

        unique_ptr<mapper_expr> parse_unit7()
        {
            return parse_binary_operator([this]() { return parse_unit6(); }, is_any_of<QString>("<<", ">>"));
        }

        unique_ptr<mapper_expr> parse_unit8()
        {
            return parse_binary_operator([this]() { return parse_unit7(); }, is_any_of<QString>("&"));
        }

        unique_ptr<mapper_expr> parse_unit9()
        {
            return parse_binary_operator([this]() { return parse_unit8(); }, is_any_of<QString>("|"));
        }

        unique_ptr<mapper_expr> parse_unit10()
        {
            return parse_binary_operator([this]() { return parse_unit9(); }, is_any_of<QString>("=", "<", "<=", ">", ">=", "!=", "<>", "is", "like", "regexp"));
        }

        unique_ptr<mapper_expr> parse_unit11()
        {
            unique_ptr<mapper_expr> inner = parse_unit10();
            if (peek_token_is("in"))
            {
                assert_next_token("in");
                assert_next_token("(");
                unique_ptr<mapper_values> values = parse_values();
                assert_next_token(")");
                return unique_ptr<mapper_expr>(new mapper_in(move(inner), move(values)));
            }
            else
                return inner;
        }

        unique_ptr<mapper_expr> parse_unit12()
        {
            unique_ptr<mapper_expr> result = parse_unit11();
            if (peek_token_is("between"))
            {
                assert_next_token("between");
                unique_ptr<mapper_expr> lhs = parse_unit11();
                assert_next_token("and");
                unique_ptr<mapper_expr> rhs = parse_unit11();
                return unique_ptr<mapper_expr>(new mapper_between(move(result), move(lhs), move(rhs)));
            }
            else
                return move(result);
        }

        unique_ptr<mapper_expr> parse_unit13()
        {
            return parse_unary_operator([this]() { return parse_unit12(); }, is_any_of<QString>("not"));
        }

        unique_ptr<mapper_expr> parse_unit14()
        {
            return parse_binary_operator([this]() { return parse_unit13(); }, is_any_of<QString>("and", "&&"));
        }

        unique_ptr<mapper_expr> parse_unit15()
        {
            return parse_binary_operator([this]() { return parse_unit14(); }, is_any_of<QString>("or", "||"));
        }
    };

} // namespace compiler

#endif // COMPILER_H
