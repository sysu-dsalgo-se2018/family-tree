#ifndef DRAGGABLETREENODE_H
#define DRAGGABLETREENODE_H
#include <QLabel>
#include <QMouseEvent>
#include <QDrag>
#include <QMimeData>
#include <iostream>
class DraggableTreeNode : public QLabel {
    using QLabel::QLabel;
protected:
    void mousePressEvent(QMouseEvent *event)
    {
        if (event->button() == Qt::LeftButton) {

            QDrag *drag = new QDrag(this);
            QMimeData *mimeData = new QMimeData;

            mimeData->setText(this->text());
            drag->setMimeData(mimeData);

            Qt::DropAction dropAction = drag->exec();
            std::cout << dropAction << std::endl;
        }
    }
};

#endif // DRAGGABLETREENODE_H
