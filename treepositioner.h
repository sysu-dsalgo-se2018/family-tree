#ifndef TREEPOSITIONER_H
#define TREEPOSITIONER_H

#include "TreeConverter.h"
#include "Person.h"
#include <QGraphicsScene>

class TreePositioner : public QObject {
    Q_OBJECT
public:
    tree_model<uint, Person> model;
    void traverse(const tree_model<uint, Person> &model, const tree_view_model<uint> &tree_model, const tree_view_model<uint>::node_t *node, int node_width, int node_height);
    void updateView();
    void updateViewLater();
    void setScene(QGraphicsScene* scene);
    uint getSelectedNode() const;
    uint maxId = 0;
signals:
    void broadcastNodeSelected(uint node);
    void broadcastNodeFiltered(const QList<uint>& nodes);
    void updated();
public slots:
    void onNewNodeDropped(uint parent, uint child);
    void onNodeSelected(uint node);
    void onNodeRemoved(uint node);
protected:
    virtual bool event(QEvent *event) override;
private:
    int node_width = 140, node_height = 50;

    QGraphicsScene *scene;
    uint selectedNode;
    bool hasSelected = false;
};

#endif // TREEPOSITIONER_H
