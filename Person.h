#ifndef PERSON_H
#define PERSON_H

#include <QString>

enum class Gender
{
    MALE = 0,
    FEMALE,
    OTHER
};

struct Person
{
    uint id;
    QString name;
    Gender gender;
    uint age;

    bool operator<(const Person &b) const
    {
        return age < b.age;
    }
};

#endif // PERSON_H
