#ifndef TREE_CONVERTER_H
#define TREE_CONVERTER_H

#include "TreeViewModel.h"
#include "TreeModel.h"

template <typename K, typename T, typename CoordT = int>
typename tree_view_model<K, CoordT>::node_t *convert(const typename tree_model<K, T>::node_t *model_node, CoordT node_width, CoordT node_height)
{
    typedef typename tree_view_model<K, CoordT>::node_t node_t;

    node_t *p = new node_t(model_node->id, node_width, node_height);

    std::vector<std::pair<T, node_t *>> children;

    for (auto &ch : model_node->child)
    {
        children.emplace_back(ch->data, convert<K, T, CoordT>(ch, node_width, node_height));
    }

    sort(children.begin(), children.end());

    for (size_t i = 0; i < children.size(); ++i)
    {
        children[i].second->parent = p;
        if (i == 0) p->first_child = children[i].second;
        if (i > 0) children[i].second->left_sibling = children[i - 1].second;
        if (i + 1 < children.size()) children[i].second->right_sibling = children[i + 1].second;
    }

    return p;
}

template <typename K, typename T, typename CoordT = int>
typename tree_view_model<K, CoordT>::node_t *convert(const tree_model<K, T> &model, const K &root, CoordT node_width = CoordT(), CoordT node_height = CoordT())
{
    if (!model.nodes.count(root)) return nullptr;

    return convert<K, T, CoordT>(model.nodes.at(root), node_width, node_height);
}

template <typename K, typename T, typename CoordT = int>
typename tree_view_model<K, CoordT>::node_t *convert(const tree_model<K, T> &model, CoordT node_width = CoordT(), CoordT node_height = CoordT())
{
    return convert(model, model.find_root(), node_width, node_height);
}

#endif // TREE_CONVERTER_H
