#ifndef TREEMODELSTORAGE_H
#define TREEMODELSTORAGE_H

#include "TreeModel.h"
#include "Person.h"
#include <QFile>
#include <QJsonDocument>
#include <QTextStream>
#include <QDebug>

QVariant toVariant(const tree_model<uint, Person>::node_t *node)
{
    QVariantMap res;
    res.insert("id", node->data.id);
    res.insert("name", node->data.name);
    res.insert("gender", static_cast<int>(node->data.gender));
    res.insert("age", node->data.age);

    if (node->child.size())
    {
        QVariantList children;
        for (auto &p : node->child)
            children.append(toVariant(p));
        res.insert("children", children);
    }

    return res;
}

QString saveToText(const tree_model<uint, Person> &model, uint maxId)
{
    QVariant tree = toVariant(model.nodes.at(model.find_root()));
    QVariantMap root;
    root.insert("tree", tree);
    root.insert("maxId", maxId);
    return QJsonDocument::fromVariant(root).toJson();
}

bool saveToFile(const tree_model<uint, Person> &model, uint maxId, QString fileName)
{
    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text))
        return false;

    QTextStream out(&file);
    out << saveToText(model, maxId) << endl;
    file.close();
    return true;
}

uint loadFrom(tree_model<uint, Person> &model, const QVariantMap &map)
{
    uint id = map["id"].toUInt();
    QString name = map["name"].toString();
    Gender gender = Gender(map["gender"].toInt());
    uint age = map["age"].toUInt();

    model.new_node(id, Person{id, name, gender, age});

    if (map.count("children"))
    {
        auto children = map["children"].toList();
        for (auto &c : children)
        {
            auto child = loadFrom(model, c.toMap());
            model.append_child(id, child);
        }
    }

    return id;
}

bool loadFromJson(tree_model<uint, Person> &model, uint &maxId, QString jsonText)
{
    QJsonParseError err;
    QJsonDocument doc = QJsonDocument::fromJson(jsonText.toUtf8(), &err);
    if (err.error == QJsonParseError::NoError)
    {
        if (doc.isObject())
        {
            model.clear();
            QVariantMap map = doc.toVariant().toMap();
            maxId = map["maxId"].toUInt();
            loadFrom(model, map["tree"].toMap());
            return true;
        }
    }

    // Json malformed
    return false;
}

bool loadFromFile(tree_model<uint, Person> &model, uint &maxId, QString fileName)
{
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text))
        return false;

    QTextStream in(&file);
    return loadFromJson(model, maxId, in.readAll());
}

#endif // TREEMODELSTORAGE_H
