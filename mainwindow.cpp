#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QApplication>
#include <QDebug>
#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QLabel>
#include <QPainter>
#include <QTextDocument>
#include <QGraphicsProxyWidget>
#include <QFileDialog>
#include <QString>

#include "draggabletreenode.h"
#include "TreeConverter.h"
#include "treenode.h"
#include "treepositioner.h"
#include "treemodelstorage.h"
#include "compiler.h"

using namespace std;

uint TREE_VIRTUAL_ROOT = 0;
uint id = 0;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setAcceptDrops(true);
    this->positioner = new TreePositioner();
    ui->lblErrmsg->setWhatsThis("Errmsg");
    connect(this->positioner, &TreePositioner::broadcastNodeSelected, this, &MainWindow::on_node_selected);
    connect(this->positioner, &TreePositioner::updated, this, &MainWindow::on_positioner_updated);
    this->positioner->model.new_node(TREE_VIRTUAL_ROOT, Person{TREE_VIRTUAL_ROOT, "virtual_root", Gender::OTHER, 0});
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setGraphicsScene(TreeScene *scene)
{
    this->scene = scene;
    this->positioner->setScene(scene);
    this->positioner->updateView();
}

void MainWindow::on_btnAdd_clicked()
{
    QString name = ui->txtAddName->text();
    Gender gender;
    if (ui->radioFemale->isChecked()) gender = Gender::FEMALE;
    else if (ui->radioMale->isChecked()) gender = Gender::MALE;
    else gender = Gender::OTHER;
    QString age = ui->txtAddAge->text();

    uint id = ++positioner->maxId;
    positioner->model.new_node_if_not_exist(id, Person{id, name, gender, age.toUInt()});
    positioner->model.append_child(TREE_VIRTUAL_ROOT, id);
    positioner->updateView();
    opstack.push(saveToText(positioner->model, positioner->maxId));
}

void MainWindow::on_txtAddName_textChanged(const QString &arg1)
{
    // ui->newNode->setText(arg1);
}


void MainWindow::on_actionOpen_triggered()
{
    QString file = QFileDialog::getOpenFileName(nullptr, "选择保存好的族谱树文件",
                        QString(),
                        "族谱树文件 (*.json)");
    if (!loadFromFile(positioner->model, positioner->maxId, file))
    {
        qDebug() << "Unable to load json file";
    }
    positioner->updateView();

    opstack = stack<QString>();
    opstack.push(saveToText(positioner->model, positioner->maxId));
}

void MainWindow::on_actionSave_triggered()
{
    QString file = QFileDialog::getSaveFileName(nullptr, "选择族谱树文件保存位置",
                        QString(),
                        "族谱树文件 (*.json)");
    saveToFile(positioner->model, positioner->maxId, file);
}

void MainWindow::on_actionDelete_triggered()
{
    uint str = positioner->getSelectedNode();
    if (!str) return;
    positioner->onNodeRemoved(str);
}

void MainWindow::on_node_selected(uint node)
{
    Person person = positioner->model.nodes[node]->data;
    ui->lblName->setText(person.name);
    ui->lblGender->setText(person.gender == Gender::FEMALE ? "女" : person.gender == Gender::MALE ? "男" : "其他");
    ui->lblAge->setText(std::to_string(person.age).c_str());
    ui->lblDesc->setText(std::to_string(positioner->model.offsprings(node)).c_str());
    ui->lblLevel->setText(std::to_string(positioner->model.level(node)).c_str());
}

struct built_in_siblings_of : public compiler::built_in_function
{
    const tree_model<uint, Person> &model;

    built_in_siblings_of(const tree_model<uint, Person> &model) : model(model) {}

    virtual compiler::object_t call(const compiler::variables &var, const std::vector<compiler::object_t> &vec) const override
    {
        if (vec.size() != 1)
            throw compiler::compilation_error("函数 siblingsOf 需要 1 个字符串，是兄弟节点的 name");
        QString sibling = to_string(vec[0]);
        return (double) model.is_sibling(sibling, var.get_data("name"));
    }
};

struct built_in_descendants_of : public compiler::built_in_function
{
    const tree_model<uint, Person> &model;

    built_in_descendants_of(const tree_model<uint, Person> &model) : model(model) {}

    virtual compiler::object_t call(const compiler::variables &var, const std::vector<compiler::object_t> &vec) const override
    {
        if (vec.size() != 1)
            throw compiler::compilation_error("函数 descendantsOf 需要 1 个字符串，是祖先节点的 name");
        QString ancestor = to_string(vec[0]);
        return (double) model.is_descendant(var.get_data("name"), ancestor);
    }
};

void MainWindow::on_btnQuery_clicked()
{
    using namespace compiler;

    try
    {
        built_in_starts_with startsWith;
        built_in_ends_with endsWith;
        built_in_siblings_of siblingsOf(positioner->model);
        built_in_descendants_of descendantsOf(positioner->model);

        vector<pair<QString, const built_in_function &>> declared_functions;
        declared_functions.push_back(make_pair("startswith", startsWith));
        declared_functions.push_back(make_pair("endswith", endsWith));
        declared_functions.push_back(make_pair("siblingsof", siblingsOf));
        declared_functions.push_back(make_pair("descendantsof", descendantsOf));

        program prog(ui->txtQuery->text());
        lexical_analyzer la(prog);
        syntax_analyzer sa(prog, la.analyze(), declared_functions);
        unique_ptr<mapper_where> result = sa.analyze();

        QList<uint> ac;

        for (const auto &it : positioner->model.nodes)
        {
            Person p = it.second->data;
            variables row;
            row.set_data("name", p.name);
            row.set_data("age", QString::number(p.age));
            row.set_data("gender", p.gender == Gender::MALE ? "male" : p.gender == Gender::FEMALE ? "female" : "other");

            if (result->map(row))
            {
                ac.append(it.first);
            }
        }

        ui->lblErrmsg->setText("");

        emit positioner->broadcastNodeFiltered(ac);
    }
    catch (const compilation_error &err)
    {
        ui->lblErrmsg->setText(err.message);
    }
    catch (const exception &err)
    {
        ui->lblErrmsg->setText(err.what());
    }
}

void MainWindow::on_actionUndo_triggered()
{
    if (opstack.size() <= 1) return;
    opstack.pop();
    loadFromJson(positioner->model, positioner->maxId, opstack.top());
    positioner->updateView();
}

void MainWindow::on_positioner_updated()
{
    opstack.push(saveToText(positioner->model, positioner->maxId));
}
