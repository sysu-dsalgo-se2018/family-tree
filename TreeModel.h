#ifndef TREE_MODEL_H
#define TREE_MODEL_H

#include <set>
#include <map>

template <typename K, typename T>
class tree_model
{
public:

    struct node_t
    {
        std::set<node_t *> child;
        node_t *parent;

        K id;
        T data;

        node_t(const K &id, const T &data)
            : parent(nullptr), id(id), data(data) {}
    };

    std::map<K, node_t *> nodes;

    ~tree_model()
    {
        clear();
    }

    void clear()
    {
        for (auto &it : nodes) delete it.second;
        nodes.clear();
    }

    void new_node_if_not_exist(const K &id, const T &data)
    {
        if (!nodes.count(id)) new_node(id, data);
    }

    void new_node(const K &id, const T &data)
    {
        node_t *p = new node_t(id, data);
        nodes[id] = p;
    }

    /**
     * 找出 model 内存储的根节点。
     * 如果没有这样的节点，则返回空。
     *
     * @return 根节点的标识符
     */
    K find_root() const
    {
        for (const auto &it : nodes)
            if (!it.second->parent)
                return it.first;
        return K();
    }

    bool is_sibling(const K &sibling, const K &cur) const
    {
        if (!nodes.count(sibling) || !nodes.count(cur))
            return false;

        node_t *sibling_node = nodes.at(sibling);
        node_t *cur_node = nodes.at(cur);
        return sibling_node->parent == cur_node->parent;
    }

    bool is_descendant(const K &descendant, const K &cur) const
    {
        if (!nodes.count(descendant) || !nodes.count(cur))
            return false;

        node_t *descendant_node = nodes.at(descendant);
        node_t *cur_node = nodes.at(cur);

        for (node_t *ptr = descendant_node; ptr; ptr = ptr->parent)
            if (ptr == cur_node)
                return true;
        return false;
    }

    /**
     * 修改 cur 为 parent 的孩子节点。
     *
     * 如果 cur 原有一个父亲节点，则旧的链接将被切断。
     * 所以本函数还可用于转接子树。
     *
     * 如果 cur 是 parent 的祖先，则函数会返回 false。
     *
     * @param parent cur 的新父亲节点
     * @param cur 要修改父亲的节点
     * @return false if the specific nodes cannot be found.
     */
    bool append_child(const K &parent, const K &cur)
    {
        if (!nodes.count(parent) || !nodes.count(cur))
            return false;
        if (parent == cur)
            return false;

        node_t *parent_node = nodes[parent];
        node_t *cur_node = nodes[cur];

        // 检查 cur 是否是 parent 的祖先，如果是则特别处理。
        for (node_t *ptr = parent_node; ptr; ptr = ptr->parent)
            if (ptr->parent == cur_node)
            {
                parent_node->parent->child.erase(parent_node);
                parent_node->parent = nullptr;

                auto p = cur_node->parent;
                if (p)
                {
                    p->child.erase(cur_node);
                    cur_node->parent = nullptr;

                    parent_node->parent = p;
                    p->child.insert(parent_node);
                }

                cur_node->parent = parent_node;
                parent_node->child.insert(cur_node);

                return true;
            }

        if (cur_node->parent)
        {
            cur_node->parent->child.erase(cur_node);
        }

        parent_node->child.insert(cur_node);
        cur_node->parent = parent_node;

        return true;
    }

    /**
     * 删除标识符为 id 的节点及其后代。
     *
     * @param id 要删除的子树的根节点的标识符
     * @return false if the specific node cannot be found.
     */
    bool remove_subtree(const K &id)
    {
        if (!nodes.count(id)) return false;
        else
        {
            node_t *p = nodes[id];
            if (p->parent)
            {
                p->parent->child.erase(p);
            }

            return remove_subtree(p);
        }
    }

    int offsprings(const K &id) const
    {
        if (!nodes.count(id))
            return -1;
        return offsprings(nodes.at(id)) - 1;
    }

    int level(const K &id) const
    {
        if (!nodes.count(id))
            return -1;
        return level(nodes.at(id));
    }

private:

    /**
     * 销毁节点 p。
     *
     * @param p 要删除的节点
     */
    void delete_node(node_t *p)
    {
        nodes.erase(p->id);
        delete p;
    }

    /**
     * 销毁节点 node 以及其后代。
     * 该函数不会维护 node 的父亲节点的 child 域。
     *
     * @param node 要销毁子树的节点
     * @return true if no errors, otherwise false.
     */
    bool remove_subtree(node_t *node)
    {
        for (auto &child : node->child)
            if (!remove_subtree(child))
                return false;

        delete_node(node);
        return true;
    }

    int offsprings(const node_t *node) const
    {
        int res = 1;
        for (auto &child : node->child)
            res += offsprings(child);
        return res;
    }

    int level(const node_t *node) const
    {
        if (!node->parent) return 0;
        return level(node->parent) + 1;
    }
};

#endif // TREE_MODEL_H
