#ifndef TREENODE_H
#define TREENODE_H

#include <QDebug>
#include <QWidget>
#include <QLabel>
#include <QMouseEvent>
#include <QDropEvent>
#include <QMimeData>
#include <QGraphicsRectItem>
#include <QApplication>
#include <QDrag>
#include <QStyle>
#include <QPalette>
#include "Person.h"

class TreeNode : public QWidget {
    Q_OBJECT
public:
    using QWidget::QWidget;
    TreeNode(int ux, int uy, int node_width, int node_height, uint key, const Person &data)
        : key(key), data(data) {
        this->setGeometry(ux, uy, node_width, node_height);
        QString display_str = data.name;
        display_str.append(QString(" (")).append(QString::fromStdString(std::to_string(data.age))).append("岁)");
        label = new QLabel(display_str);
        label->setAlignment(Qt::AlignCenter | Qt::AlignVCenter);
        label->setFont(QFont("monospace", 15));
        label->setGeometry(0, 0, node_width, node_height);
        label->setParent(this);
        label->setWhatsThis("TreeNode");
        if (data.gender == Gender::FEMALE)
            label->setProperty("gender", "female");
        else if (data.gender == Gender::MALE)
            label->setProperty("gender", "male");
        else
            label->setProperty("gender", "other");
        this->setAcceptDrops(true);
        this->setMouseTracking(true);
    }
    virtual ~TreeNode() override {
        delete label;
    }
protected:
    virtual void mouseMoveEvent(QMouseEvent *event) override {
        auto pos = event->globalPos();

        if (QLineF(start, pos).length() < QApplication::startDragDistance())
            return;

        QDrag *drag = new QDrag(this);
        QMimeData *mime = new QMimeData();
        drag->setMimeData(mime);

        mime->setText(key);

        drag->exec();
    }

    virtual void mousePressEvent(QMouseEvent *event) override {
        start = event->globalPos();

        setCursor(Qt::ClosedHandCursor);
        QFont select("monospace", 15);
        select.setBold(true);
        label->setFont(select);
    }

    virtual void mouseReleaseEvent(QMouseEvent *) override {
        emit nodeSelected(key);

        setCursor(Qt::OpenHandCursor);
    }

    virtual void keyReleaseEvent(QKeyEvent *event) override {
        if (event->key() == Qt::Key::Key_Delete)
            emit remove(key);
    }

    void dragEnterEvent(QDragEnterEvent *event) override {
        qDebug() << "Drag Enter";
        if (event->mimeData()->hasText()) {
            event->acceptProposedAction();
        }
    }

    void dragMoveEvent(QDragMoveEvent *event) override {
        event->acceptProposedAction();
    }

    void dropEvent(QDropEvent* event) override {
        qDebug() << "";
        if (event->mimeData()->hasText()) {
              event->acceptProposedAction();
              emit newNodeDropped(key, event->mimeData()->text());
              qDebug() << "Signal Emitted" << event->mimeData()->text();
        }
    }
signals:
    void newNodeDropped(const QString& parent, const QString& child);
    void nodeSelected(const QString &node);
    void remove(const QString &node);
public:
    void onNodeFiltered(const QStringList& nodes) {
        if (nodes.contains(key)) {
            label->setProperty("marked", true);
        } else {
            label->setProperty("marked", false);
        }
        updateUI();
    }
    void onNodeSelected(const QString& node) {
        if (node != key) {
            label->setProperty("selected", false);
        } else {
            label->setProperty("selected", true);
        }
        updateUI();
    }
    void updateUI() {
        label->style()->unpolish(label);
        label->style()->polish(label);
    }
private:
    QPoint start;
    QLabel* label;
    QString key;
    Person data;
};

#endif // TREENODE_H
