#include "mainwindow.h"

#include <QApplication>
#include <QGraphicsRectItem>
#include <QGraphicsView>
#include <QLabel>
#include <QPainter>
#include <QTextDocument>
#include <QGraphicsProxyWidget>
#include <QDebug>
#include "treescene.h"
#include "file_utils.h"
using namespace std;

int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    TreeScene scene;
    QGraphicsView view(&scene);
    app.setStyleSheet(readAll(":/assets/css/canvas.css"));
    view.setRenderHints(QPainter::SmoothPixmapTransform);
    view.resize(500, 500);
    view.show();

    MainWindow mainWindow;
    mainWindow.setGraphicsScene(&scene);
    mainWindow.show();

    return app.exec();
}
