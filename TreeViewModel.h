#ifndef TREE_VIEW_MODEL_H
#define TREE_VIEW_MODEL_H

#include <vector>

/**
 * 表示画布中的树节点。
 *
 * @param K 树节点标识符
 * @param CoordT 坐标单位
 */
template <typename K, typename CoordT = int>
class tree_view_model
{
public:
    typedef CoordT coord_t;

    struct node_t
    {
        node_t *parent;
        node_t *first_child;
        node_t *left_sibling;
        node_t *right_sibling;

        node_t(const K &key, coord_t width, coord_t height) :
            parent(nullptr),
            first_child(nullptr),
            left_sibling(nullptr),
            right_sibling(nullptr),
            x(), y(),
            width(width), height(height),
            level_offset(0), offset(0),
            key(key) {}

        coord_t x, y;
        coord_t width, height;
        node_t *left_neighbor;

        /**
         * 到该层节点的左边距。
         */
        double level_offset;

        /**
         * 孩子节点到父亲节点所在子树的同层节点的左边界的距离。
         */
        double offset;

        K key;

        inline bool leaf() const
        {
            return first_child == nullptr;
        }
    };

    std::vector<node_t *> prevs;

    // ======== inputs =========

    coord_t node_width, node_height;
    coord_t subtree_separation, sibling_separation, level_separation;

public:

    // ======== setters ========

    void set_default_node_size(coord_t width, coord_t height)
    {
        node_width = width;
        node_height = height;
    }

    void set_subtree_separation(coord_t subtree_separation)
    {
        this->subtree_separation = subtree_separation;
    }

    void set_sibling_separation(coord_t sibling_separation)
    {
        this->sibling_separation = sibling_separation;
    }

    void set_level_separation(coord_t level_separation)
    {
        this->level_separation = level_separation;
    }

private:

    node_t *get_prev_node_at_level(unsigned level)
    {
        return level < 0 || level >= prevs.size() ? nullptr : prevs[level];
    }

    void init_prev_node_at_level()
    {
        prevs.clear();
    }

    void set_prev_node_at_level(unsigned level, node_t *node)
    {
        if (level >= prevs.size())
            prevs.resize(level + 1, nullptr);
        prevs[level] = node;
    }

    /**
     * 返回节点正对树朝向的宽度。
     * 如果朝向为纵向，返回节点的宽度；
     * 如果朝向为横向，返回节点的高度。
     *
     * @param node 要计算宽度的节点
     * @return 节点正对树朝向的宽度
     */
    double node_separation(node_t *node)
    {
        return node->width ? node->width : node_width;
    }

    /**
     * 返回节点 now 的第 depth 层的后代中中最左侧的那个节点。
     *
     * @param now 当前节点
     * @param depth 需要找到的节点的层数
     * @param level 当前遍历到了哪层
     * @return 节点 now 的第 depth 层的后代中中最左侧的那个节点
     */
    node_t *get_left_most(node_t *now, unsigned depth, unsigned level = 0)
    {
        if (level == depth) return now;
        else if (now->leaf() || level > depth) return nullptr;
        else
        {
            node_t *leftmost, *rightmost;

            for (leftmost = get_left_most(rightmost = now->first_child, depth, level + 1);
                !leftmost && rightmost->right_sibling;
                leftmost = get_left_most(rightmost = rightmost->right_sibling, depth, level + 1));

            return leftmost;
        }
    }

    /**
     * 调整节点 now 的子树与其左侧兄弟节点的子树横坐标以保证没有重叠。
     *
     * 因为在 first_walk 中我们已经将子树根节点紧密排列，因此不存在
     * 两个子树间距过宽的情况。因此我们只考虑处理重叠的情况。
     *
     * 我们通过为每一层计算右侧子树至少有的左边界（通过左侧子树的右边界
     * 计算得到）和右侧子树目前的实际边界，然后根据差值调整子树的位置。
     * 左侧子树的边界由右侧子树最左侧的点的左侧相邻节点决定，而这些节点
     * 并不一定在 now 的左侧相邻兄弟节点的子树中，在所有 now 的
     * 左侧相邻节点的子树中都可能存在。
     *
     * @param now 待处理的节点
     */
    void apportion(node_t *now)
    {
        node_t *leftmost = now->first_child;
        node_t *neighbor = leftmost->left_neighbor;
        unsigned compare_depth = 1;

        while (leftmost && neighbor)
        {
            // 计算当前层最左侧的节点和其左侧的不同子树的相邻节点的坐标
            double neighbor_offset = 0;  // 左侧子树在当前层距离 now 的父亲节点的子树的左边界的偏移量
            double leftmost_offset = 0; // 当前子树在当前层距离 now 的父亲节点的子树的左边界的偏移量
            node_t *ancestor_neighbor = neighbor; // 遍历左侧子树的前 compare_depth 层节点
            node_t *ancestor_leftmost = leftmost; // 遍历当前子树的前 compare_depth 层节点

            // 遍历 now 到 compare_depth 层节点从而计算
            // compare_depth 层节点的两个子树的偏移量。
            for (unsigned i = 0; i < compare_depth; ++i)
            {
                ancestor_leftmost = ancestor_leftmost->parent;
                ancestor_neighbor = ancestor_neighbor->parent;
                leftmost_offset += ancestor_leftmost->offset;
                neighbor_offset += ancestor_neighbor->offset;
            }

            // 计算右侧子树应为了避免重叠而右移的距离
            double distance =
                (neighbor->level_offset + neighbor_offset + node_separation(neighbor) + subtree_separation) -
                (leftmost->level_offset + leftmost_offset);

            // distance < 0 表示子树偏移距离过多，这个是因为在其他地方会需要这么大的偏移
            if (distance > 0) // 如果需要右移
            {
                unsigned left_siblings = 0;
                node_t *p;
                for (p = now; p && p != ancestor_neighbor; p = p->left_sibling)
                    left_siblings++;

                if (p)
                {
                    // 将添加的间隔分散到之前的所有兄弟节点使得绘制出的树间距均衡。
                    double portion = distance / left_siblings;
                    for (p = now; p != ancestor_neighbor; p = p->left_sibling)
                    {
                        p->level_offset += distance;
                        p->offset += distance;
                        distance -= portion;
                        break;
                    }
                }
                else
                {
                    // apportion(now) 只处理 now 节点的子树及其兄弟的子树的重叠
                    // 如果 p == nullptr 说明 ancestor_neighbor 与 now 不共
                    // 享父亲节点。因此该冲突应该遍历到 ancestor_neighbor 和
                    // now 的最近公共祖先时再处理。
                    return;
                }
            }

            // 找到下一层右侧子树的最左侧节点 leftmost
            // 和其左侧的某个子树的最右的节点 neighbor
            compare_depth++;
            if (leftmost->leaf())
                leftmost = get_left_most(now, compare_depth);
            else
                leftmost = leftmost->first_child;
            neighbor = !leftmost ? nullptr : leftmost->left_neighbor;
        }
    }

    /**
     * 计算每个节点相对于父亲节点所在的子树的偏移。
     *
     * node->offset 用于将子树进行偏移
     * node->level_offset 用于计算在子树内的相对位移
     * 因此节点的横坐标就是 level_offset 和 offset 的和。
     *
     * @param now 当前计算坐标的节点
     * @param level 节点的深度
     * @return tree if no errors, otherwise false
     */
    bool first_walk(node_t *now, unsigned level = 0)
    {
        now->left_neighbor = get_prev_node_at_level(level);

        set_prev_node_at_level(level, now);

        if (now->leaf())
        {
            if (now->left_sibling)
            {
                // 该叶子结点位于左兄弟节点的右侧，距离为 sibling_separation
                now->level_offset = now->left_sibling->level_offset + node_separation(now->left_sibling) + sibling_separation;
            }
            else
            {
                // 最左侧的叶子节点距离本层左边框距离为 0
                now->level_offset = 0;
            }
        }
        else
        {
            node_t *leftmost, *rightmost;

            if (!first_walk(leftmost = rightmost = now->first_child, level + 1))
                return false;

            // 遍历该节点的所有子树
            while (rightmost->right_sibling)
            {
                if (!first_walk(rightmost = rightmost->right_sibling, level + 1))
                    return false;
            }

            // 节点置于孩子节点的中轴线上
            double mid = (leftmost->level_offset + rightmost->level_offset) / 2;

            if (now->left_sibling)
            {
                // 在 apportion(now) 中调整 now->level_offset
                now->level_offset = now->left_sibling->level_offset + node_separation(now->left_sibling) + sibling_separation;
                now->offset = now->level_offset - mid;

                // 调整 now 的节点位置
                apportion(now);
            }
            else
            {
                now->level_offset = mid;
            }
        }
        return true;
    }

    /**
     * 通过 level_offset 和 offset 计算节点的坐标。
     * offset 表示子树的偏移量，level_offset 表示节点在子树层内的左边距，因此
     * 节点的横坐标就是 sum(offset) + level_offset。
     * 由于 offset 表示的孩子节点到 now 的父亲的子树的左边距，因此当前节点的
     * sum(offset) = sum(ancestor->offset)，也就是 now 的祖先的 offset
     * 的和，而不包括自己的 offset。
     *
     * @param now 当前要计算坐标的节点
     * @param offset 累计的子树偏移量
     * @param level 当前节点的深度，用于计算纵坐标
     * @return true if no errors, otherwise false.
     */
    bool second_walk(node_t *now, double offset = 0, unsigned level = 0)
    {
        now->x = offset + now->level_offset;
        now->y = level * level_separation;

        // Apply the modifier value for this node to all its children.
        if (!now->leaf())
        {
            if (!second_walk(now->first_child, offset + now->offset, level + 1))
                return false;
        }

        if (now->right_sibling)
        {
            if (!second_walk(now->right_sibling, offset, level))
                return false;
        }

        return true;
    }

public:

    /**
     * 计算树中节点在画布上的坐标。
     *
     * @param root 要绘制的树的根节点
     * @return true if no errors, otherwise returns false.
     */
    bool position(node_t *root)
    {
        if (!root) return true;

        init_prev_node_at_level();

        if (!first_walk(root))
            return false;
        if (!second_walk(root))
            return false;

        return true;
    }
};

#endif // TREE_VIEW_MODEL_H
