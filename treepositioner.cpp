#include "TreeConverter.h"
#include "treenode.h"
#include "treepositioner.h"

#include <QDebug>

#include <queue>

using namespace std;

void TreePositioner::traverse(const tree_model<uint, Person> &model, const tree_view_model<uint> &tree_model, const tree_view_model<uint>::node_t *root, int node_width, int node_height)
{
    typedef const typename tree_view_model<uint>::node_t node_t;

    if (!root) return;

    queue<node_t *> que;
    que.push(root);

    while (!que.empty())
    {
        node_t *u = que.front(); que.pop();

        for (node_t *ch = u->first_child; ch; ch = ch->right_sibling)
        {
            if (u != root)
                scene->addLine(u->x + node_width / 2, u->y - tree_model.level_separation + node_height / 2, ch->x + node_width / 2, ch->y - tree_model.level_separation + node_height / 2);

            que.push(ch);
        }

        // Skip virtual root
        if (u == root) continue;
        TreeNode *node = new TreeNode(u->x, u->y - tree_model.level_separation, node_width, node_height, u->key, model.nodes.at(u->key)->data);
        connect(node, &TreeNode::newNodeDropped, this, &TreePositioner::onNewNodeDropped);
        connect(node, &TreeNode::nodeSelected, this, &TreePositioner::onNodeSelected);
        connect(node, &TreeNode::remove, this, &TreePositioner::onNodeRemoved);
        connect(this, &TreePositioner::broadcastNodeSelected, node, &TreeNode::onNodeSelected);
        connect(this, &TreePositioner::broadcastNodeFiltered, node, &TreeNode::onNodeFiltered);
        scene->addWidget(node);
    }
}

void TreePositioner::updateView() {
    tree_view_model<uint> view_model;
    view_model.set_level_separation(2 * node_height);
    view_model.set_default_node_size(node_width, node_height);
    view_model.set_sibling_separation(node_width);
    view_model.set_subtree_separation(node_width);
    tree_view_model<uint>::node_t *root = convert(model, node_width, node_height);
    if (!view_model.position(root)) {
        qDebug() << "failed";
    }
    else {
        scene->clear();
        traverse(model, view_model, root, node_width, node_height);
    }
}

void TreePositioner::updateViewLater() {
    QCoreApplication::postEvent(this, new QEvent(QEvent::Type::UpdateRequest));
}

void TreePositioner::setScene(QGraphicsScene* scene) {
    this->scene = scene;
}

uint TreePositioner::getSelectedNode() const
{
    return hasSelected ? selectedNode : 0;
}

void TreePositioner::onNewNodeDropped(uint parent, uint child) {
    qDebug() << "New Node was Dropped" << parent << "to" << child;
    if (!model.append_child(parent, child))
        qDebug() << "failed";
    else
    {
        emit updated();
        updateViewLater();
    }
}

void TreePositioner::onNodeSelected(uint node) {
    qDebug() << "Node" << node << "was selected";
    selectedNode = node;
    hasSelected = true;
    emit broadcastNodeSelected(node);
}

void TreePositioner::onNodeRemoved(uint node) {
    qDebug() << "Node" << node << "was deleted";
    if (!hasSelected) return;
    hasSelected = false;

    if (model.remove_subtree(selectedNode))
    {
        emit updated();
        updateViewLater();
    }
    else
        qDebug() << "Failed to remove subtree of " << node << endl;
}

bool TreePositioner::event(QEvent *event)
{
    if (event->type() == QEvent::Type::UpdateRequest)
    {
        updateView();
        return true;
    }

    return false;
}
