#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "TreeModel.h"
#include "treescene.h"
#include "treepositioner.h"
#include <QMainWindow>
#include <stack>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void setGraphicsScene(TreeScene *scene);

private slots:
    void on_btnAdd_clicked();

    void on_txtAddName_textChanged(const QString &arg1);

    void on_actionOpen_triggered();

    void on_actionSave_triggered();

    void on_actionDelete_triggered();

    void on_node_selected(uint node);

    void on_btnQuery_clicked();

    void on_actionUndo_triggered();

    void on_positioner_updated();

private:
    Ui::MainWindow *ui;
    TreeScene *scene;
    TreePositioner* positioner;
    std::stack<QString> opstack;
};

#endif // MAINWINDOW_H
