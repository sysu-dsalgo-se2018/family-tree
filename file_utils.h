#ifndef FILE_UTILS_H
#define FILE_UTILS_H

#include <QFile>
#include <QTextStream>

QString readAll(QString file)
{
    QFile f(file);
    f.open(QIODevice::ReadOnly);
    QTextStream s(&f);
    return s.readAll();
}

#endif // FILE_UTILS_H
